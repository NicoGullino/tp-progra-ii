package TpImplementacion;

public class Turno {
	private Integer dia;
	private Integer mes;
	private Integer anio;
	private Integer hora;
	private Integer minutos;

	Turno(Integer dia, Integer mes, Integer anio, Integer hora, Integer minutos){
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		this.hora = hora;
		this.minutos = minutos;
	}
	
}
