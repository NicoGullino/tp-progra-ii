package TpImplementacion;

public class Persona {
	private String nombre;
	private Integer edad;
	private Integer dni;
	private String tipoDeVotante;
	
	Persona(String nombre, Integer edad, Integer dni, String tipoVotante){
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		this.tipoDeVotante = tipoVotante;
	}
	
	private boolean tieneEnfermedadPrexistente(Persona persona) {
		if(!persona.tipoDeVotante.equals("enfermedad prexistente")) {
			return false;
		}
		return true;
	}
	
	private boolean trabajadorEnElDia(Persona persona) {
		if(!persona.tipoDeVotante.equals("trabajador")){
			return false;
		}
		return true;
	}
	
	private boolean esMayor(Persona persona) {
		if(persona.edad <= 65) {
			return true;
		}
		return false;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getTipoDeVotante() {
		return tipoDeVotante;
	}

	public void setTipoDeVotante(String tipoDeVotante) {
		this.tipoDeVotante = tipoDeVotante;
	}
	
}
