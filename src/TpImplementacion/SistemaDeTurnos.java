package TpImplementacion;

import java.util.HashMap;
import java.util.Set;

import javax.management.RuntimeErrorException;

public class SistemaDeTurnos {
	Set<Mesa> mesas;
	Mesa unaMesa;
	HashMap <Integer, Persona> personasVotantes;
	
	
	public SistemaDeTurnos(String nombreDeSistema) {
		if(nombreDeSistema.equals(null)) {
			throw new RuntimeException("Nombre Invalido");
		}
		
	}
	
	
	public void registrarVotante(int dni, String nombre, int edad, boolean enfPrevia, boolean trabaja) {
		if(!this.personasVotantes.containsKey(dni)){
			throw new RuntimeException("No existe el votante");
		}
		if(edad <= 16) {
			throw new RuntimeException("El votante no es mayor de 16 a�os");
		}
		
		else {
			if(enfPrevia == true) {
				Persona votante = new Persona(nombre, edad, dni, "enfermedad prexistente");
				this.personasVotantes.put(dni, votante);
			}
			else if(trabaja == true) {
				Persona votante = new Persona(nombre, edad, dni, "trabajador");
				this.personasVotantes.put(dni, votante);
			}
			
			
		}
		
	}
	
	public int agregarMesa(String tipoMesa, int dni) {
		if(!this.personasVotantes.containsKey(dni)) {
			throw new RuntimeException("no esta registrado");
		}
		
		if(!this.mesas.contains(tipoMesa)) {
			throw new RuntimeException("Tipo de mesa invalido...");
		}
		this.unaMesa.settipoDeMesa(tipoMesa);
		this.unaMesa.getPresidente().setDni(dni);
		this.mesas.add(unaMesa);
		return unaMesa.getNumeroMesa();
	}
	
	public Tupla<Integer, Integer> asignarTurno(int dni){
		if(!this.personasVotantes.containsKey(dni)) {
			throw new RuntimeException("El dni ingresado no existe...");
		}
		if(this.mesas.contains(personasVotantes.containsKey(dni))) {
			throw new RuntimeException("el votante ya tiene turno asignado:" + );
		}
		
		
	}

	
	
}
