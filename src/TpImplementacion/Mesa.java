package TpImplementacion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public abstract class Mesa {
	private Persona presidente;
	private Integer franjahoraria;
	private Set<Persona>votantes;
	private Set<Turno>turnos;
	private String tipoDeMesa;
	private Integer numeroMesa;
	private Integer cupos;
	
	public Mesa(Persona presidente,Integer franjahoraria,String tipoDeMesa,Integer cupos, Integer numeroMesa) {
		this.presidente = presidente;
		this.franjahoraria = franjahoraria;
		this.tipoDeMesa = tipoDeMesa;
		this.cupos = cupos;
		this.votantes = new HashSet<Persona>();
		this.turnos=new HashSet<Turno>();
		this.numeroMesa = numeroMesa;
		
	}
	
	public Mesa(String tipoMesa, Integer dni) {
		this.tipoDeMesa = tipoMesa;
		this.presidente.setDni(dni);
	}
	
	public boolean voto(Integer dni) {
		return this.votantes.contains(dni);
	}
	public boolean mesaDisponible(String tipoMesa, ArrayList<Mesa> mesas) {
		if(mesas.contains(tipoMesa) && mesas.g > 0) {
			return true;
		}
		return false;
	}
	
	
	boolean consultarTurno(Turno turnoDisponible) {
		return this.turnos.contains(turnoDisponible);
	}

	void asignarPresidenteMesa(Persona presidente, Mesa mesa) {
		if(!tipoDeMesa.equals(mesa.tipoDeMesa)) {
			 throw new RuntimeException("La mesa no existe...");
		}
		if(presidente.getDni() == this.presidente.getDni())
			mesa.presidente = presidente;
	}
	
	
	
	public abstract void limiteCupos();
	
	
	//getters y setters:
	public Persona getPresidente() {
		return presidente;
	}
	public void setPresidente(Persona presidente) {
		this.presidente = presidente;
	}
	public Integer getFranjahoraria() {
		return franjahoraria;
	}
	public void setFranjahoraria(Integer franjahoraria) {
		this.franjahoraria = franjahoraria;
	}
	public Set<Persona> getVotantes() {
		return votantes;
	}
	public void setVotantes(Set<Persona> votantes) {
		this.votantes = votantes;
	}
	public Set<Turno> getTurnos() {
		return turnos;
	}
	public void setTurnos(Set<Turno> turnos) {
		this.turnos = turnos;
	}
	public Integer getNumeroMesa() {
		return numeroMesa;
	}
	public void settipoDeMesa(String tipoDeMesa) {
		this.tipoDeMesa = tipoDeMesa;
	}
	public Integer getCupos() {
		return cupos;
	}
	public void setCupos(Integer cupos) {
		this.cupos = cupos;
	}

	public String getTipoDeMesa() {
		return tipoDeMesa;
	}

	public void setTipoDeMesa(String tipoDeMesa) {
		this.tipoDeMesa = tipoDeMesa;
	}

	public void setNumeroMesa(Integer numeroMesa) {
		this.numeroMesa = numeroMesa;
	}
	
}
